import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;
import java.util.regex.Pattern;

public class Monom {
    private int grad;
    private int coef;
    public Monom(){}
    public Monom(int grad){ this.grad=grad;}
    public Monom(int grad,int coef){
        this.grad=grad;
        this.coef=coef;
    }

  public Monom readMonom(String s){
      /*user gives the input as a string and we need a monomial
       *this method separates the string into two parts, the coefficient and the degree and then converts them into integers
       *and returns the monomial */
       String[] spl = s.split(Pattern.quote("x^"));
       this.coef=Integer.parseInt(spl[0]);
       this.grad=Integer.parseInt(spl[1]);
      return this;
  }

   public String writeMonom(){
      /*turns a monomial into a String
       */
      String s=new String();
        if(this.coef>=0) {
            s = ("+"+ this.coef + "x^" + this.grad );}
            else{
            s=(this.coef+"x^" + this.grad );
        }
      return s;
   }

  public Monom addMonom(Monom m1,Monom m2){
      //since they are monomials we assume they have the same degree
      this.coef=m1.coef+m2.coef;
      this.grad= m2.grad;
      return this;
  }
  public Monom substractMonom(Monom m1,Monom m2){
      //assume again they have the same degree
      if(m1.grad==m2.grad){
          this.coef= m1.coef-m2.coef;
      this.grad=m1.grad;}
      return this;
  }
  public Monom multiplyMonom(Monom m1,Monom m2){
      //can have different degrees
      this.coef= m1.coef*m2.coef;
      this.grad=m1.grad+m2.grad;
      return this;
  }
    public Monom deriv(){
      this.coef=this.coef*this.grad;
        this.grad=this.grad-1;
      return this;
    }

    public void setCoef(int coef) {
        this.coef = coef;
    }

    public void setGrad(int grad) {
        this.grad = grad;
    }

    public int getCoef() {
        return coef;
    }

    public int getGrad() {
        return grad;
    }
}
