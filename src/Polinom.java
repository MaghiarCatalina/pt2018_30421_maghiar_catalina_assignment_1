import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

import static java.lang.Integer.parseInt;

public class Polinom {
    ArrayList<Monom> poli = new ArrayList<>();
    public Polinom(){}
    public Polinom(ArrayList<Monom> poli){this.poli=poli;}

    public String writePoly(){
        /* returns the polynomial as a string
        *the input is the polynomial stored in an arrayList of monomials
         */
        String polystring = new String();
        for(int i=0;i<this.poli.size();i++){
            polystring=polystring+this.poli.get(i).writeMonom(); }
        return polystring;
    }
    public String addPoly(Polinom p1,Polinom p2){
        /* We assume the first polynomial is the one with the biggest degree(or equal to the second)
         *we go through the ArrayLists and add the terms with the same degree
         * if there isn't another term with the same degree in the other polynomial
         * the if statement is not entered, so the variable ad(added) remains 0
         * then check if the term was added, if not(so ad=0) we add it to the result
         * result is returned as a string
         */

        for(int i=0;i<p1.poli.size();i++) {
            int ad = 0;
            for(int j=0;j<p2.poli.size();j++) {
                if (p1.poli.get(i).getGrad() == p2.poli.get(j).getGrad()){
                    Monom toBeAdded = new Monom();
                    this.poli.add(toBeAdded.addMonom(p1.poli.get(i),p2.poli.get(j)));
                    ad= 1;
                }
            }
            if(ad==0){this.poli.add(p1.poli.get(i));}
        }
        return this.writePoly();
    }
    public String substractPoly(Polinom p1,Polinom p2){
        /* We assume the first polynomial is the one with the biggest degree(or equal to the second)
         *we go through the ArrayLists and add the terms with the same degree
         * if there isn't another term with the same degree in the other polynomial
         * the if statement is not entered, so the variable ad(added) remains 0
         * then check if the term was added, if not(so ad=0) we add it to the result
         * result is returned as a string
         */

        for(int i=0;i<p1.poli.size();i++) {
            int ad = 0;
            for(int j=0;j<p2.poli.size();j++) {
                if (p1.poli.get(i).getGrad() == p2.poli.get(j).getGrad()){
                    Monom toBeAdded = new Monom();
                    this.poli.add(toBeAdded.substractMonom(p1.poli.get(i),p2.poli.get(j)));
                    ad= 1;
                }
            }
            if(ad==0){this.poli.add(p1.poli.get(i));}
        }
        return this.writePoly();
    }
    public String multiplyPoly(Polinom p1,Polinom p2){
        Polinom temp = new Polinom();
        for(int i=0;i<p1.poli.size();i++) {
            for (int j = 0; j < p2.poli.size(); j++) {
                Monom toBeAdded = new Monom();
                temp.poli.add(toBeAdded.multiplyMonom(p1.poli.get(i),p2.poli.get(j)));
            }
        }
        for(int i=0;i<temp.poli.size();i++) {
            int ad = 0;
            int t=0;
            for(int j=i+1;j<temp.poli.size();j++) {
                if (temp.poli.get(i).getGrad() == temp.poli.get(j).getGrad()){
                    Monom var = new Monom();
                    var.addMonom(temp.poli.get(i),temp.poli.get(j));
                    t++;
                    while (var.getGrad()==temp.poli.get(t+j).getGrad()){
                        var.addMonom(var,temp.poli.get(t+j));
                        t++;
                    }
                    this.poli.add(var);
                    ad= 1;
                }
            }
            i=i+t;
            if( ad==0){this.poli.add(temp.poli.get(i));}
        }
        return this.writePoly();
    }

    public String derivPoly(Polinom p){
        //uses the first polynomial
        for(int i=0;i<p.poli.size();i++) {
            this.poli.add(p.poli.get(i).deriv());
        }
        return this.writePoly();
    }
}
