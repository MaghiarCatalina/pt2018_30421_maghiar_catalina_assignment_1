
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;

public class GUI {
    public static void main(String[] args){
        ArrayList<Monom> polynomial1 = new ArrayList<>();
        ArrayList<Monom> polynomial2 = new ArrayList<>();
        Polinom poly1 = new Polinom(polynomial1);  //first polynomial
        Polinom poly2 = new Polinom(polynomial2); //second polynomial
        Polinom rzAdd = new Polinom();  //polynomial resulted after addition
        Polinom rzSub = new Polinom();  //polynomial resulted after subtraction
        Polinom rzDeriv = new Polinom(); //polynomial resulted after derivation

        Frame f = new Frame("Calculator");
         TextField insertTf = new TextField();
         insertTf.setBounds(180,50,100,20); //where the terms of polynomials are introduced one by one
         TextField p1 = new TextField();
         p1.setBounds(180,100,150,20); //where the first polynomial is written(can not be edited)
         p1.setEditable(false);
         TextField p2 = new TextField();
         p2.setBounds(180,150,150,20); //where the second polynomial is written(can not be edited)
        TextField resTf = new TextField();
        p2.setEditable(false);
        resTf.setBounds(180,200,150,20); //where the result of the operations will be showed

        JLabel ins = new JLabel("Insert term: ");
        ins.setBounds(50,50,100,20);
        Button pb1 = new Button("Polynomial 1");
        pb1.setBounds(50,100,100,20);
        Button pb2 = new Button("Polynomial 2");
        pb2.setBounds(50,150,100,20);
        Button plus = new Button("+");
        plus.setBounds(50,250,30,30);
        Button minus = new Button("-");
        minus.setBounds(100,250,30,30);
        Button deriv = new Button("deriv");
        deriv.setBounds(150,250,60,30);
        JLabel result = new JLabel("Result: ");
        result.setBounds(50,200,100,20);
        Button cancel = new Button("Cancel");
        cancel.setBounds(50,400,70,20);
        Button reset = new Button("Reset");
        reset.setBounds(170,400,70,20);

        cancel.addActionListener(new ActionListener() {
            //exit the program
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        reset.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                resTf.setText("");
            }
        });
        pb1.addActionListener(new ActionListener() {
            /*when pressed the following things happen:
            *the term in the insert text field is added to the first polynomial
            * it appears in the corresponding text field
            * insert test field is cleared
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                Monom m =new Monom();
                poly1.poli.add(m.readMonom(insertTf.getText()));
                p1.setText(poly1.writePoly());
                insertTf.setText(null);
            }
        });
        pb2.addActionListener(new ActionListener() {
            /*when pressed the following things happen:
            *the term in the insert text field is added to the second polynomial
            * it appears in the corresponding text field
            * insert test field is cleared
            */
            @Override
            public void actionPerformed(ActionEvent e) {
                Monom m =new Monom();
                poly2.poli.add(m.readMonom(insertTf.getText()));
                p2.setText(poly2.writePoly());
                insertTf.setText(null);
            }
        });
        p1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });
        p2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });
        plus.addActionListener(new ActionListener() {
            /* when clicked, the result of the addition is showed in the result text field
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                resTf.setText(rzAdd.addPoly(poly1,poly2));
            }
        });
        minus.addActionListener(new ActionListener() {
            /* when clicked, the result of the subtraction is showed in the result text field
            */
            @Override
            public void actionPerformed(ActionEvent e) {
                resTf.setText(rzSub.substractPoly(poly1,poly2));
            }
        });
        deriv.addActionListener(new ActionListener() {
            /* when clicked, the result of the derivation is showed in the result text field
            */
            @Override
            public void actionPerformed(ActionEvent e) {
                resTf.setText(rzDeriv.derivPoly(poly1));
            }
        });
        f.add(ins);
        f.add(insertTf);
        f.add(pb1);
        f.add(pb2);
        f.add(result);
        f.add(p1);
        f.add(p2);
        f.add(resTf);
        f.add(plus);
        f.add(minus);
        f.add(deriv);
        f.add(cancel);
        f.add(reset);
        f.setSize(500,500);
        f.setBackground(Color.lightGray);
        f.setLayout(null);
        f.setVisible(true);
    }
}
